### Historical background

To Do

### Context

To Do

## Content of the directory

- Regular 
    - OTF file
    - bmap Mac fontfile
- Plaque 
    - OTF file
    - bmap Mac fontfile

- Fontlog

## Known issues and future developments

To Do

## Coverage

- DIV-Regular currently provides the following Unicode coverage:
    - Basic Latin: 57/95 
    - Latin-1 Supplement: 54/96

- DIV-Plaque currently provides the following Unicode coverage:
    - Basic Latin: 57/95 
    - Latin-1 Supplement: 54/96

## Information for Contributors

Copyright (c) 1999 Hammerfonts for Normal

## ChangeLog

To do

## Acknowledgements

If you make modifications be sure to add your name (N), email (E), web-address (W) and description (D).  
This list is sorted by last name in alphabetical order. 

N: Pierre Huyghebaert  
E: pierre@speculoos.com<br/>
W: http://www.speculoos.com<br/>
D: Typography